﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    AnimatedSprite animator;
    public GameEvent OnRespawn;
    public GameEvent OnGameover;
    public FloatObject MaxLives;
    public FloatObject Lives;
    public GameObject lifePrefab;
    public RectTransform parent;
    List<RectTransform> lifeSprite = new List<RectTransform>();
    private void Start()
    {
        animator = GetComponent<AnimatedSprite>();
        SpawntLives();
    }
    public void SpawntLives()
    {
        for (int i = 0; i < Lives.Value; i++)
        {
            var p = Instantiate(lifePrefab).GetComponent<RectTransform>();
            p.SetParent(parent);
            p.localScale = new Vector3(14, 14, 1);
            p.localPosition = (Vector3.right + new Vector3(55f * (i + 1) + 40f, 0));
            lifeSprite.Add(p);
        }
    }
    public void ResetLives()
    {
        Lives.Value = MaxLives.Value;
    }
    public void RemoveLife()
    {
        var s = lifeSprite[lifeSprite.Count - 1];
        Lives.Value -= 1;
        lifeSprite.Remove(s);
        Destroy(s.gameObject);
        if (lifeSprite.Count == 0)
            OnGameover.Raise();
        else
            StartCoroutine(Respawn());
    }
    public void OnDied()
    {
        animator.Play(0);
        RemoveLife();
    }

    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(1);
        animator.Stop();
        OnRespawn.Raise();
    }
    private void OnApplicationQuit()
    {
        ResetLives();
    }
}
