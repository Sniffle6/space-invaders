﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Float", menuName = "Variables/Float")]
public class FloatObject : ScriptableObject
{
    public float Value;
    public void SetToText(Text text)
    {
        text.text = Value.ToString("n0");
    }
    public void SetValue(float value)
    {
        Value = value;
    }
    public void Save(string prefName = null)
    {
        if (prefName == null)
            prefName = this.name;
        PlayerPrefs.SetFloat(prefName, Value);
    }
    public void Load(string prefName = null)
    {
        if (prefName == null)
            prefName = this.name;
        Value = PlayerPrefs.GetFloat(prefName, 0);
    }
    public void IncrementValue(float value)
    {
        Value += value;
    }
    public void UploadFloat(string name_)
    {
        KongregateAPIBehaviour.instance.SubmitStat(name_, this);
    }
}
