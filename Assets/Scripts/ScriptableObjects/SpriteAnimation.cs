﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "New Sprite Animation", menuName = "Animationss/Sprite")]
public class SpriteAnimation : ScriptableObject
{
    //public bool SpawnAsNewObject;
    public int startState;
    [HideInInspector]
    public bool UseGlobalSpeed;
    [HideInInspector]
    public bool UnifromSpeed;
    [HideInInspector]
    public float speed;
    [HideInInspector]
    public float[] animationSpeed;
    public bool DisableAfterPlay;
    public bool DestroyAfterPlay;
    [HideInInspector]
    public Sprite[] List = new Sprite[1];


}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(SpriteAnimation))]
public class SpriteAnimationEditor : Editor
{
    private SpriteAnimation Target { get { return (target as SpriteAnimation); } }
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        EditorGUI.BeginChangeCheck();

        Target.UseGlobalSpeed = EditorGUILayout.Toggle("Global Speed:", Target.UseGlobalSpeed);
        if (!Target.UseGlobalSpeed)
        {
            Target.UnifromSpeed = EditorGUILayout.Toggle("Uniform Speed:", Target.UnifromSpeed);
            if (Target.UnifromSpeed)
                Target.speed = EditorGUILayout.FloatField("Speed:", Target.speed);
        }
        else
        {
            Target.UnifromSpeed = false;
        }

        int count = 0;
        count = EditorGUILayout.DelayedIntField("Number of Animating Sprites", Target.List != null ? Target.List.Length : 0);
        if (count < 0)
            count = 0;

        if (count == 0)
            return;

        if (Target.List == null || Target.List.Length != count)
            Array.Resize<Sprite>(ref Target.List, count);
        if (Target.animationSpeed == null || Target.animationSpeed.Length != count)
            Array.Resize<float>(ref Target.animationSpeed, count);

        EditorGUILayout.LabelField("Place sprites shown based on the order of animation.");
        EditorGUILayout.Space();

        for (int i = 0; i < count; i++)
        {
            Target.List[i] = (Sprite)EditorGUILayout.ObjectField("Sprite " + (i + 1), Target.List[i], typeof(Sprite), false, null);
            if (!Target.UseGlobalSpeed && !Target.UnifromSpeed)
                Target.animationSpeed[i] = EditorGUILayout.FloatField("Speed:", Target.animationSpeed[i]);
        }

        if(GUILayout.Button(new GUIContent("Open Preview Window")))
        {
            SpriteAnimationPreview window = (SpriteAnimationPreview)EditorWindow.GetWindow(typeof(SpriteAnimationPreview));
            window.Show();
            window.SetPreviewAnimation(Target);
        }

        if (EditorGUI.EndChangeCheck())
        {
            // Tile.GetComponent<SpriteRenderer>().sprite = Tile.Animation[Tile.CurrentAnimation].List[Tile.state];
            EditorUtility.SetDirty(Target);
        }
    }
}


public class SpriteAnimationPreview : EditorWindow
{
    SpriteAnimation PreviewAnimation;
    int state = 0;
    int counter = 0;
    int scale = 100;
    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Animation/Sprite Preview")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        SpriteAnimationPreview window = (SpriteAnimationPreview)EditorWindow.GetWindow(typeof(SpriteAnimationPreview));
        window.Show();
    }

    void OnGUI()
    {
        EditorGUI.BeginChangeCheck();
        PreviewAnimation = (SpriteAnimation)EditorGUILayout.ObjectField("Animation", PreviewAnimation, typeof(SpriteAnimation), false, null);
        if (EditorGUI.EndChangeCheck())
        {
            state = 0;
            counter = 0;
        }
        scale = (int)EditorGUILayout.Slider("Preview Scale", scale, 25, Screen.width < (Screen.height-100) ? Screen.width*2 : Screen.height-100);
        if (PreviewAnimation)
        {
            if (PreviewAnimation.UseGlobalSpeed)
                EditorGUILayout.FloatField("Global Speed(Preview)", 120);
            else
            {
                if (PreviewAnimation.UnifromSpeed)
                    PreviewAnimation.speed = EditorGUILayout.FloatField("Frame Time(Seconds)", PreviewAnimation.speed);
                else
                {
                    for (int i = 0; i < PreviewAnimation.animationSpeed.Length; i++)
                    {
                        PreviewAnimation.animationSpeed[i] = EditorGUILayout.FloatField("Frame Time "+i+"(Seconds)", PreviewAnimation.animationSpeed[i]);
                    }
                }
            }
            if (PreviewAnimation.List[state])
            {
                var texture = AssetPreview.GetAssetPreview(PreviewAnimation.List[state]);
                texture = Resize(texture,  scale/2,  scale/2);
                GUILayout.Label(texture);
            }
        }

    }
    float cooldown = 0;
    private void Update()
    {
        if (!PreviewAnimation)
            return;
        counter++;
        int multiplier = 120;
        if (PreviewAnimation.UseGlobalSpeed)
        {
            if (Application.isPlaying)
                cooldown = Globals.Speed * multiplier;
            else
                cooldown = multiplier;
        }
        else
        {
            if (PreviewAnimation.UnifromSpeed)
                cooldown = PreviewAnimation.speed * multiplier;
            else
                cooldown = PreviewAnimation.animationSpeed[state] * multiplier;
        }
        if (counter > cooldown)
        {
            state++;
            if (state > PreviewAnimation.List.Length - 1)
                state = 0;
            Repaint();
            counter = 0;
        }
    }

    public Texture2D Resize(Texture2D source, int newWidth, int newHeight)
    {


        float w = source.width;
        float h = source.height;
        float ratio = 1;
        if (w > h)
            ratio = w / h;
        if (h > w)
            ratio = h / w;
        float sw = scale * ratio;


        source.filterMode = FilterMode.Point;
        RenderTexture rt = RenderTexture.GetTemporary(w > h ? (int)(newWidth * ratio) : newWidth, w < h ? (int)(newHeight * ratio) : newHeight);
        rt.filterMode = FilterMode.Point;
        RenderTexture.active = rt;
        Graphics.Blit(source, rt);
        var nTex = new Texture2D( w > h ?  (int)(newWidth * ratio) : newWidth, w < h ? (int)(newHeight * ratio) : newHeight);
        nTex.ReadPixels(new Rect(0, 0, w > h ? (int)(newWidth * ratio) : newWidth, w < h ? (int)(newHeight * ratio) : newHeight), 0, 0);
        nTex.Apply();
        RenderTexture.active = null;
        return nTex;

    }
    public void SetPreviewAnimation(SpriteAnimation preview)
    {
        state = 0;
        counter = 0;
        PreviewAnimation = preview;
    }
}
#endif