﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AnimatedSprite : MonoBehaviour
{

    public int state;
    public bool PlayOnStart;
    public bool PlayRandom;

    public int StartAnimation;
    public int CurrentAnimation = 0;

    public SpriteAnimation[] Animation;
    bool Animating;
    new SpriteRenderer renderer;
    public Sprite defaultSprite;
    private void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
        if (PlayRandom)
            StartAnimation = UnityEngine.Random.Range(0, Animation.Length);
        CurrentAnimation = StartAnimation;
        state = Animation[StartAnimation].startState;
        if (PlayOnStart)
        {
            renderer.sprite = Animation[StartAnimation].List[state];
            Animating = true;
            StartCoroutine(Animator());
        }
        defaultSprite = renderer.sprite;
    }
    public void Play(int index)
    {
        if (!gameObject.activeSelf)
            return;

        //if (Animation[index].SpawnAsNewObject)
        //{
        //    var o = new GameObject();
        //    o.transform.position = transform.position;
        //    var s = o.AddComponent<SpriteRenderer>();
        //    var a = o.AddComponent<AnimatedSprite>();
        //    Array.Resize<SpriteAnimation>(ref a.Animation, 1);
        //    a.Animation[0] = Animation[index];
        //    a.StartAnimation = 0;
        //    a.CurrentAnimation = 0;
        //    s.sprite = a.Animation[0].List[0];
        //    a.Animating = true;
        //    a.StartCoroutine(a.Animator());
        //}
        //else
        //{
            StopCoroutine(Animator());
            state = Animation[StartAnimation].startState;
            CurrentAnimation = index;
            renderer.sprite = Animation[CurrentAnimation].List[state];
            Animating = true;
            StartCoroutine(Animator());
       // }
    }
    public void Stop()
    {
        Animating = false;
        StopCoroutine(Animator());
    }
    IEnumerator Animator()
    {
        while (Animating)
        {
            yield return new WaitForSeconds(Animation[CurrentAnimation].UseGlobalSpeed ? Globals.Speed : Animation[CurrentAnimation].UnifromSpeed ? Animation[CurrentAnimation].speed : Animation[CurrentAnimation].animationSpeed[state]);
            state++;
            if (state > Animation[CurrentAnimation].List.Length - 1)
            {
                state = 0;
                if (Animation[CurrentAnimation].DisableAfterPlay)
                    gameObject.SetActive(false);
                if (Animation[CurrentAnimation].DestroyAfterPlay)
                    Destroy(gameObject);
            }
            renderer.sprite = Animation[CurrentAnimation].List[state];
        }
        renderer.sprite = defaultSprite;
    }

}


#if UNITY_EDITOR
[CustomEditor(typeof(AnimatedSprite))]
public class AnimatedSpriteEditor : Editor
{
    private AnimatedSprite Target { get { return (target as AnimatedSprite); } }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        EditorGUI.BeginChangeCheck();

        if (Target.Animation.Length == 0)
            return;
        if (Target.Animation[Target.CurrentAnimation] == null)
            return;
        Target.Animation[Target.CurrentAnimation].UseGlobalSpeed = EditorGUILayout.Toggle("Global Speed:", Target.Animation[Target.CurrentAnimation].UseGlobalSpeed);
        if (!Target.Animation[Target.CurrentAnimation].UseGlobalSpeed)
        {
            Target.Animation[Target.CurrentAnimation].UnifromSpeed = EditorGUILayout.Toggle("Uniform Speed:", Target.Animation[Target.CurrentAnimation].UnifromSpeed);
            if (Target.Animation[Target.CurrentAnimation].UnifromSpeed)
                Target.Animation[Target.CurrentAnimation].speed = EditorGUILayout.FloatField("Speed:", Target.Animation[Target.CurrentAnimation].speed);
        }
        else
        {
            Target.Animation[Target.CurrentAnimation].UnifromSpeed = false;
        }

        int count = 0;
        if (Target.Animation.Length != 0 && Target.Animation[Target.CurrentAnimation] != null)
            count = EditorGUILayout.DelayedIntField("Number of Animating Sprites", Target.Animation[Target.CurrentAnimation].List != null ? Target.Animation[Target.CurrentAnimation].List.Length : 0);
        if (count < 0)
            count = 0;

        if (count == 0)
            return;

        if (Target.Animation[Target.CurrentAnimation].List == null || Target.Animation[Target.CurrentAnimation].List.Length != count)
            Array.Resize<Sprite>(ref Target.Animation[Target.CurrentAnimation].List, count);

        if (Target.Animation[Target.CurrentAnimation].animationSpeed == null || Target.Animation[Target.CurrentAnimation].animationSpeed.Length != count)
            Array.Resize<float>(ref Target.Animation[Target.CurrentAnimation].animationSpeed, count);


        EditorGUILayout.LabelField("Place sprites shown based on the order of animation.");
        EditorGUILayout.Space();

        for (int i = 0; i < count; i++)
        {
            Target.Animation[Target.CurrentAnimation].List[i] = (Sprite)EditorGUILayout.ObjectField("Sprite " + (i + 1), Target.Animation[Target.CurrentAnimation].List[i], typeof(Sprite), false, null);
            if (!Target.Animation[Target.CurrentAnimation].UseGlobalSpeed && !Target.Animation[Target.CurrentAnimation].UnifromSpeed)
                Target.Animation[Target.CurrentAnimation].animationSpeed[i] = EditorGUILayout.FloatField("Speed:", Target.Animation[Target.CurrentAnimation].animationSpeed[i]);
        }

        if (Target.state > Target.Animation[Target.CurrentAnimation].List.Length - 1)
            Target.state = Target.Animation[Target.CurrentAnimation].List.Length - 1;
        if (Target.state < 0)
            Target.state = 0;
        if (EditorGUI.EndChangeCheck())
        {
            // Tile.GetComponent<SpriteRenderer>().sprite = Tile.Animation[Tile.CurrentAnimation].List[Tile.state];
            EditorUtility.SetDirty(Target);
        }
    }
}
#endif

