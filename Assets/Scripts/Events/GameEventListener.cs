﻿
using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{

    public bool FireOnStart;
    public GameEvent Event;
    public UnityEvent Responce;
    private void Start()
    {
        if (FireOnStart)
            OnEventRaised();
    }
    private void OnEnable()
    {
        Event.RegisterListener(this);
    }
    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }
    public void OnEventRaised()
    {
        try
        {
            if (GetComponent<Canvas>())
            {
                if (GetComponent<Canvas>().enabled)
                    Responce.Invoke();
            }
            else
                Responce.Invoke();
        }
        catch
        {
            Debug.LogError("On event raised Invoke missing, Event: " + Event.name + ", EventListener: " + gameObject.name);
        }
    }
}
