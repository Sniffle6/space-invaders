﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public static Vector2 direction;
    public static Action OnFire;
    // Update is called once per frame
    private void Update()
    {
        if (Input.GetAxisRaw("Horizontal") > 0)
            direction = Vector2.right;
        else if (Input.GetAxisRaw("Horizontal") < 0)
            direction = Vector2.left;
        else
            direction = Vector2.zero;

        if (Input.GetButton("Jump"))
        {
            if (OnFire != null)
                OnFire.Invoke();
        }
    }
}
