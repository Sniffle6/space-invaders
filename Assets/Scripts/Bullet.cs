﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Bullet : MonoBehaviour
{
    public float speed;
    [HideInInspector]
    public Vector3 direction;
    Rigidbody2D rb;


    public FloatObject score;
    public FloatObject currentSessionScore;
    public FloatObject highScore;
    public FloatObject MysteryKills;

    public GameEvent OnScoreGained;
    public GameEvent OnHighScoreGained;

    public AudioSource a_Source;

    public GameEvent OnDied;
    public string IgnoreTag;
    public GameObject explosionPrefab;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    public GameObject Construct(Vector3 position, Vector3 dir, string ignore)
    {
        transform.position = position;
        direction = dir;
        IgnoreTag = ignore;
        if (a_Source)
            a_Source.Play();
        return this.gameObject;
    }
    private void FixedUpdate()
    {
        rb.MovePosition(transform.position + direction * speed * Time.deltaTime);
        if (Mathf.Abs(transform.position.y) >= 18f)
        {
            Explode(transform.position);
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(IgnoreTag))
            return;
        switch (collision.gameObject.tag)
        {
            case "Alien":
                IncrementScore(collision.gameObject.GetComponent<Enemy>().ScoreGainedOnKill);
                collision.gameObject.GetComponent<Enemy>().Remove();
                Destroy(this.gameObject);
                break;
            case "MysteryShip":
                IncrementScore(250 * ((int)MysteryKills.Value));
                collision.gameObject.GetComponent<MysteryShip>().Explode(250 * ((int)MysteryKills.Value));
                MysteryKills.Value++;
                Destroy(this.gameObject);
                break;
            case "Player":
                OnDied.Raise();
                Destroy(this.gameObject);
                break;
            case "Bullet":
                Destroy(collision.gameObject);
                Explode(collision.transform.position);
                Destroy(this.gameObject);
                break;
            case "Bunker":
                var mat = collision.gameObject.GetComponent<Renderer>().material;
                var state = mat.GetInt("_State");
                if (state < 4)
                {
                    mat.SetInt("_State", state + 1);
                    switch (state + 1)
                    {
                        case 1:
                            mat.SetFloat("_Threshold", Random.Range(0.25f, 0.6f));
                            break;
                        case 2:
                            mat.SetFloat("_Threshold", Random.Range(0.65f, 0.75f));
                            break;
                        case 3:
                            mat.SetFloat("_Threshold", Random.Range(0.8f, 0.94f));
                            break;
                        case 4:
                            collision.gameObject.SetActive(false);
                            break;
                    }
                }
                Destroy(this.gameObject);
                break;

        }
    }
    private void Explode(Vector3 position)
    {
        if (explosionPrefab)
        {
            var o = Instantiate(explosionPrefab);
            o.transform.position = position;
        }
    }
    private void OnDisable()
    {

        if (a_Source)
            a_Source.Stop();
    }
    public void IncrementScore(int value)
    {
        score.Value += value;
        OnScoreGained.Raise();
        if (score.Value > highScore.Value)
        {
            highScore.Value = score.Value;
            OnHighScoreGained.Raise();
        }
        if(score.Value > currentSessionScore.Value)
        {
            currentSessionScore.Value = score.Value;
            currentSessionScore.UploadFloat("Highscore");
        }
    }
}
