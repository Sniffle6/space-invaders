﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }
    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this.gameObject);
        else
            Instance = this;
    }
    public FloatObject score;
    public UnityEvent OnStart;
    public UnityEvent OnComplete;
    public UnityEvent OnQuit;
    private void Start()
    {
        if (OnStart != null)
            OnStart.Invoke();
    }
    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }
    public void LevelComplete()
    {
        Globals.LevelSpeed -= 0.1f;
        OnComplete.Invoke();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void ResetGame()
    {
        Globals.LevelSpeed = 1;
        score.Value = 0;
        SceneManager.LoadScene(0);
    }
    private void OnApplicationQuit()
    {
        OnQuit.Invoke();
    }
}
