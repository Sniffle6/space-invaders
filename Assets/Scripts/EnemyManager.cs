﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class EnemyManager : MonoBehaviour
{
    public GameObject TopEnemyPrefab;
    public GameObject MiddleEnemyPrefab;
    public GameObject BottomEnemyPrefab;
    public GameObject MysteryShipPrefab;
    MysteryShip mysteryShip = null;
    [Space(20)]
    public AudioSource audioSource;
    public AudioClip HeartBeat;


    [Space(20)]
    public int ColumnLength;
    public int RowLength;

    [Space(20)]
    public float X_SPACE_BETWEEN_ENEMY;
    public float Y_SPACE_BETWEEN_ENEMY;

    [Space(20)]
    public int START_X;
    public int START_Y;
    

    [Space(20)]
    public GameEvent OnDied;

    List<GameObject> enemies = new List<GameObject>();
    List<GameObject> activeEnemies = new List<GameObject>();

    Vector2 direction = Vector2.right;


    bool Moving;
    bool moveDown;
    float downCount;

    // Start is called before the first frame update
    void Start()
    {
        for (var i = 0; i < ColumnLength * RowLength; i++)
        {
            switch (i / ColumnLength)
            {
                case 0:
                    enemies.Add(Instantiate(TopEnemyPrefab));
                    break;
                case 1:
                    enemies.Add(Instantiate(MiddleEnemyPrefab));
                    break;
                case 2:
                    enemies.Add(Instantiate(MiddleEnemyPrefab));
                    break;
                case 3:
                    enemies.Add(Instantiate(BottomEnemyPrefab));
                    break;
                case 4:
                    enemies.Add(Instantiate(BottomEnemyPrefab));
                    break;
            }

            enemies[i].transform.position = new Vector2(START_X + X_SPACE_BETWEEN_ENEMY * (i % ColumnLength), START_Y + -Y_SPACE_BETWEEN_ENEMY * (i / ColumnLength));
        }
        Moving = true;
        StartCoroutine(Movement());
        InvokeRepeating(nameof(SpawnMysteryShip), 1, 1);
    }
    void UpdateActiveEnemies()
    {
        activeEnemies.Clear();
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i].activeSelf)
                activeEnemies.Add(enemies[i]);
        }
        if (activeEnemies.Count == 0)
        {
            LevelManager.Instance.LevelComplete();
        }
    }
    void DetermineSpeed()
    {
        float maxSpeed = 0.01f;
        int leftRow = 100;
        int rightRow = -1;
        int mostLeft = 0;
        int mostRight = 10;
        int leftDiff, rightDiff = 0;
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i].activeSelf)
            {
                if (i % ColumnLength < leftRow)
                    leftRow = i % ColumnLength;
                if (i % ColumnLength > rightRow)
                    rightRow = i % ColumnLength;
            }
        }
        leftDiff = leftRow - mostLeft;
        rightDiff = mostRight - rightRow;

        // float speed = 1 + ((float)activeEnemies.Count - downCount - (leftDiff+rightDiff)) / 40;
        float speed = Globals.LevelSpeed - ((float)(enemies.Count - activeEnemies.Count) + (float)downCount + (float)leftDiff + (float)rightDiff) * 0.01f;
        if (speed < maxSpeed)
            speed = maxSpeed;
        Globals.Speed = speed;
    }
    float pitchCounter = 1;

    public bool EnemiesPaused { get; private set; }

    IEnumerator Movement()
    {
        while (Moving)
        {
            UpdateActiveEnemies();
            DetermineSpeed();
            yield return new WaitForSeconds(Globals.Speed);
            for (var i = 0; i < activeEnemies.Count; i++)
            {
                if (direction == Vector2.right && (activeEnemies[i].transform.position + Vector3.right).x > 35)
                {
                    moveDown = true;
                    direction = Vector2.left;
                }
                if (direction == Vector2.left && (activeEnemies[i].transform.position + Vector3.left).x < -35)
                {
                    moveDown = true;
                    direction = Vector2.right;
                }
            }
            if (moveDown)
            {
                for (var i = activeEnemies.Count - 1; i >= 0; i--)
                {
                    if ((activeEnemies[i].transform.position - Vector3.up).y < -13)
                    {
                        Moving = false;
                        OnDied.Raise();
                        break;
                    }
                    activeEnemies[i].transform.Translate(Vector2.down);
                }
                downCount++;
                moveDown = false;
            }
            else
                for (var i = 0; i < activeEnemies.Count; i++)
                    activeEnemies[i].transform.Translate(direction);
            audioSource.pitch = pitchCounter;
            audioSource.PlayOneShot(HeartBeat);
            pitchCounter -= 0.1f;
            if (pitchCounter < 0.6f)
                pitchCounter = 1;
        }
    }
    private void SpawnMysteryShip()
    {
        if (Random.Range(0.0f, 1.0f) >= 0.9f && mysteryShip == null && !EnemiesPaused)
        {
            mysteryShip = Instantiate(MysteryShipPrefab).GetComponent<MysteryShip>();
            mysteryShip.transform.position = new Vector3(35, 14);
        }
    }
    public void DisableEnemies()
    {
        EnemiesPaused = true;
        if (mysteryShip)
            mysteryShip.SetMove(false);
        for (int i = 0; i < enemies.Count; i++)
            enemies[i].SetActive(false);
    }
    public void StopEnemies()
    {
        EnemiesPaused = true;
        if (mysteryShip)
            mysteryShip.SetMove(false);
        for (int i = 0; i < enemies.Count; i++)
            enemies[i].GetComponent<Enemy>().PauseCorutine();
        Moving = false;
        StopAllCoroutines();
    }
    public void StartEnemies()
    {
        EnemiesPaused = false;
        if (mysteryShip)
            mysteryShip.SetMove(true);
        for (int i = 0; i < enemies.Count; i++)
            enemies[i].GetComponent<Enemy>().PlayCorutine();
        Moving = true;
        StartCoroutine(Movement());
    }
   
}