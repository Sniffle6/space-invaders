﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TypeText : MonoBehaviour
{
    public float speed;
    string TextToType;
    bool TypingText;
    Text text;
    public int StartDelay = 0;
    public int CompleteDelay = 0;
    public UnityEvent OnComplete;
    private IEnumerator coroutine;
    void OnEnable()
    {
        text = GetComponent<Text>();
        TextToType = text.text;
        text.text = "";
        Invoke(nameof(InvokeCorutine), StartDelay);
    }
    private void InvokeCorutine()
    {
        TypingText = true;
        coroutine = Typing();
        StartCoroutine(coroutine);
    }
    private void OnDisable()
    {
        text.text = TextToType;
        TypingText = false;
        StopCoroutine(coroutine);
    }
    IEnumerator Typing()
    {
        int count = 0;
        int maxCount = TextToType.Length - 1;
        while (TypingText)
        {
            yield return new WaitForSeconds(speed);
            count++;
            if (count + 1 < TextToType.Length && char.IsWhiteSpace(TextToType[count]))
                count++;
            if (count > TextToType.Length)
            {
                Invoke(nameof(InvokeOnComplete), CompleteDelay);
                TypingText = false;
                break;
            }
            text.text = TextToType.Substring(0, count);
        }
    }
    private void InvokeOnComplete()
    {
        OnComplete.Invoke();
    }
}
