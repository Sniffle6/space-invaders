﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int ScoreGainedOnKill;
    public GameObject bulletPrefab;
    public GameObject explosionPrefab;
    public bool PauseFire;
    GameObject bullet;
    int layerMask = 1 << 9;
    bool FireBullet;
    IEnumerator corutine;
    AnimatedSprite animator;
    AudioSource a_Source;

    private void Start()
    {
        a_Source = GetComponent<AudioSource>();
        animator = GetComponent<AnimatedSprite>();
        FireBullet = true;
        corutine = Fire();
        StartCoroutine(corutine);
    }
    public void PauseCorutine()
    {
        PauseFire = true;
        animator.Stop();
    }
    public void PlayCorutine()
    {

        PauseFire = false;
        animator.Play(0);
    }
    IEnumerator Fire()
    {
        while (FireBullet)
        {
            yield return new WaitForSeconds(Random.Range(1.0f, 10.0f));
            if (PauseFire)
            {
                yield return new WaitUntil(() => PauseFire == false);
                yield return new WaitForSeconds(Random.Range(1.0f, 10.0f));
            }
            if (Random.Range(0.0f, 1.0f) >= 0.5f)
            {
                RaycastHit2D hit = Physics2D.Raycast((Vector2)transform.position + Vector2.down, Vector2.down, Mathf.Infinity, layerMask);
                if (hit.collider == null)
                {
                    bullet = Instantiate(bulletPrefab).GetComponent<Bullet>().Construct(
                        transform.position - new Vector3(0, 1.5f, 0),
                        Vector3.down, "Alien");
                }
            }

        }
    }
    public void Remove()
    {
        var o = Instantiate(explosionPrefab);
        o.transform.position = transform.position;
        //animator.Play(1);
        //a_Source.Play();
        gameObject.SetActive(false);
    }
}
