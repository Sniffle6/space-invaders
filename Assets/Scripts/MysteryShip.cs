﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MysteryShip : MonoBehaviour
{
    public float speed;
    bool CanMove = true;
    AudioSource a_Source;
    public GameObject explodePrefab;
    public GameObject scorePrefab;
    private void Start()
    {
        a_Source = GetComponent<AudioSource>();
    }
    void Update()
    {
        if (!CanMove)
            return;
        
        a_Source.panStereo = transform.position.x / 35;
        transform.Translate(Vector3.left * speed * Time.deltaTime);

        if (transform.position.x < -35)
            Destroy(this.gameObject);
    }
    public void Explode(int value)
    {
        Instantiate(explodePrefab, transform.position, Quaternion.identity);
        Instantiate(scorePrefab, transform.position, Quaternion.identity).GetComponentInChildren<Text>().text = value.ToString("n0");
        Destroy(gameObject);
    }
    public void SetMove(bool value)
    {
        CanMove = value;
        if (!CanMove)
            a_Source.Stop();
        else
            a_Source.Play();
    }
}
