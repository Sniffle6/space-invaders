﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public GameObject bulletPrefab;
    GameObject bullet;
    private void Start()
    {
        PlayerInput.OnFire += Fire;
    }
    private void OnDisable()
    {
        PlayerInput.OnFire -= Fire;
    }
    public void StopFire()
    {
        PlayerInput.OnFire -= Fire;
    }
    public void StartFire()
    {
        PlayerInput.OnFire += Fire;
    }
    public void Fire()
    {
        if (bullet != null)
            return;
        bullet = Instantiate(bulletPrefab).GetComponent<Bullet>().Construct(transform.position + new Vector3(0, 1.5f, 0), Vector3.up, "Player");
    }
}
