﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class KongregateAPIBehaviour : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void KAPIInit();
    [DllImport("__Internal")]
    private static extern void SubmitKongStat(string name_, int value_);

    public static KongregateAPIBehaviour instance;
    public void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        gameObject.name = "KongregateAPI";

        if (Application.isEditor)
            return;
        KAPIInit();


        //  Application.ExternalEval(
        //    @"if(typeof(kongregateUnitySupport) != 'undefined'){
        //  kongregateUnitySupport.initAPI('KongregateAPI', 'OnKongregateAPILoaded');
        //};"
        //  );
    }

    public void OnKongregateAPILoaded(string userInfoString)
    {
        OnKongregateUserInfo(userInfoString);
    }

    public void OnKongregateUserInfo(string userInfoString)
    {
        var info = userInfoString.Split('|');
        var userId = System.Convert.ToInt32(info[0]);
        var username = info[1];
        var gameAuthToken = info[2];
        Debug.Log("Kongregate User Info: " + username + ", userId: " + userId);
    }

    public void SubmitStat(string StatName, FloatObject StatValue)
    {
        if (Application.isEditor)
            return;
        SubmitKongStat(StatName, (int)StatValue.Value);
    }
}
