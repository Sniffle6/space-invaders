﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public bool CanMove;
    private void Start()
    {
        CanMove = true;
    }
    public void SetCanMove(bool value)
    {
        CanMove = value;
    }
    public void MoveToo(int value)
    {
        transform.position = new Vector3(value, transform.position.y);
    }
    void Update()
    {
        if (!CanMove)
            return;
        if (transform.position.x < -35 && PlayerInput.direction == Vector2.left)
            return;
        if (transform.position.x > 35 && PlayerInput.direction == Vector2.right)
            return;
        transform.Translate(PlayerInput.direction * speed * Time.deltaTime);
    }

}
