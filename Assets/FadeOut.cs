﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour
{
    public float speed;
    bool Fading;
    IEnumerator corutine;
    Text text;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        Invoke(nameof(StartFade), 0.25f);
    }
    public void StartFade()
    {
        Fading = true;
        corutine = Fade();
        StartCoroutine(Fade());
    }
    IEnumerator Fade()
    {
        Color color = text.color;
        float alpha = color.a;
        while (Fading)
        {
            yield return new WaitForEndOfFrame();
            text.color = new Color(text.color.r, text.color.g, text.color.b, alpha-= speed);
            GetComponent<RectTransform>().Translate(Vector3.up * Time.deltaTime * 2);
            if (alpha <= 0)
                Destroy(gameObject);
        }
    }
}
